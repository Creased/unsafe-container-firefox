/*!
 * Author: Baptiste MOINE <contact@bmoine.fr>
 * Credits: Based on https://addons.mozilla.org/firefox/addon/facebook-container
 * Project: Unsafe Container
 * Homepage: http://www.bmoine.fr/
 * Released: 12/07/2018
 * Description: Unsafe Container is based on the Multi-Account Container addon and places
 *              all your web activity in a container named "unsafe container" and allows
 *              you to move it afterwards.
 */

// Container info
const CONTAINER_NAME = "Unsafe";
const CONTAINER_COLOR = "red";
const CONTAINER_ICON = "circle";

// Multi-Account Container Addon
const MAC_ADDON_ID = "@testpilot-containers";
let macAddonEnabled = false;

let cookieStoreId = null;

const canceledRequests = {};
const tabsWaitingToLoad = {};

// Check if Multi-Account Container is enabled
async function isMACAddonEnabled() {
  try {
    const macAddonInfo = await browser.management.get(MAC_ADDON_ID);
    if (macAddonInfo.enabled) {
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

// Setup Multi-Account Container module listeners (enabled, disabled, installed, uninstalled)
async function setupMACAddonManagementListeners() {
  browser.management.onInstalled.addListener(info => {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = true;
    }
  });
  browser.management.onUninstalled.addListener(info => {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = false;
    }
  });
  browser.management.onEnabled.addListener(info => {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = true;
    }
  });
  browser.management.onDisabled.addListener(info => {
    if (info.id === MAC_ADDON_ID) {
      macAddonEnabled = false;
    }
  });
}

// Check if Multi-Account Container has assigned a specific container to the provided URL
async function getMACAssignment(url) {
  if (macAddonEnabled) { // MAC is enabled?
    try {
      const assignment = await browser.runtime.sendMessage(MAC_ADDON_ID, {
        method: "getAssignment",
        url
      });
      return assignment;
    } catch (e) {
      return false;
    }
  } else {
    return false;
  }
}

// Cancel the HTTP request
function cancelRequest(tab, options) {
  // we decided to cancel the request at this point, register canceled request
  canceledRequests[tab.id] = {
    requestIds: {
      [options.requestId]: true
    },
    urls: {
      [options.url]: true
    }
  };

  // since webRequest onCompleted and onErrorOccurred are not 100% reliable
  // we register a timer here to cleanup canceled requests, just to make sure we don't
  // end up in a situation where certain urls in a tab.id stay canceled
  setTimeout(() => {
    if (canceledRequests[tab.id]) {
      delete canceledRequests[tab.id];
    }
  }, 2000);
}

// Ask to cancel a request to prevent opening two tabs
function shouldCancelEarly(tab, options) {
  // we decided to cancel the request at this point
  if (!canceledRequests[tab.id]) {
    cancelRequest(tab, options);
  } else {
    let cancelEarly = false;
    if (canceledRequests[tab.id].requestIds[options.requestId] ||
        canceledRequests[tab.id].urls[options.url]) {
      // same requestId or url from the same tab
      // this is a redirect that we have to cancel early to prevent opening two tabs
      cancelEarly = true;
    }
    // register this requestId and url as canceled too
    canceledRequests[tab.id].requestIds[options.requestId] = true;
    canceledRequests[tab.id].urls[options.url] = true;
    if (cancelEarly) {
      return true;
    }
  }
  return false;
}

// Use existing container, or create a new one
async function setupContainer() {
  const contexts = await browser.contextualIdentities.query({name: CONTAINER_NAME});

  if (contexts.length > 0) {
    cookieStoreId = contexts[0].cookieStoreId;
  } else {
    const context = await browser.contextualIdentities.create({
      name: CONTAINER_NAME,
      color: CONTAINER_COLOR,
      icon: CONTAINER_ICON
    });
    cookieStoreId = context.cookieStoreId;
  }
}

// Open and close tab with a new cookieStorId
function reopenTab({url, tab, cookieStoreId}) {
  browser.tabs.create({
    url,
    cookieStoreId,
    active: tab.active,
    index: tab.index,
    windowId: tab.windowId
  });
  browser.tabs.remove(tab.id);
}

// Check if we need to re-open the tab
async function checkTab(tab) {
  if ((tab.url).startsWith("http")) {
    const macAssigned = await getMACAssignment(tab.url);
    if (! macAssigned && tab.cookieStoreId === "firefox-default") {
      reopenTab({
        url: tab.url,
        tab,
        cookieStoreId
      });
    }
  }
}

// Tab changed state since it was loading, check if we can process it
function tabUpdatedCallback(tabId, changeInfo, tab) {
  if (changeInfo.url && tabsWaitingToLoad[tabId]) {
    // Tab we're waiting for switched it's url, maybe we reopen
    delete tabsWaitingToLoad[tabId];
    checkTab(tab);
  }
  if (tab.status === "complete" && tabsWaitingToLoad[tabId]) {
    // Tab we're waiting for completed loading
    delete tabsWaitingToLoad[tabId];
  }
  if (!Object.keys(tabsWaitingToLoad).length) {
    // We're done waiting for tabs to load, remove event listener
    browser.tabs.onUpdated.removeListener(tabUpdatedCallback);
  }
}

// Check if we need to reopen current tabs
async function processCurrentTabs() {
  const tabs = await browser.tabs.query({});
  tabs.map(async tab => {
    if (!tab.incognito) { // We can't process incognito tabs since we aren't able to change cookieStoreId in private browsing...
      if (tab.url === "about:blank") {
        if (tab.status === "loading") {
          tabsWaitingToLoad[tab.id] = true;
          if (!browser.tabs.onUpdated.hasListener(tabUpdatedCallback)) {
            browser.tabs.onUpdated.addListener(tabUpdatedCallback);
          }
        }
      } else {
        checkTab(tab);
      }
    }
  });
}

// Process new tab
async function containUnsafe(options) {
  if (options.tabId === -1) {
    // Request doesn't belong to a tab
    return;
  }

  if (tabsWaitingToLoad[options.tabId]) {
    // Cleanup just to make sure we don't get a race-condition with startup reopening
    delete tabsWaitingToLoad[options.tabId];
  }

  const tab = await browser.tabs.get(options.tabId);

  // We have to check with every request if the tab is assigned with MAC
  const macAssigned = await getMACAssignment(options.url);
  if (macAssigned || tab.cookieStoreId !== "firefox-default") {
    // This tab is assigned with MAC, so we don't handle this request
    return;
  }

  if (tab.incognito) {
    // We don't handle incognito tabs
    return;
  }

  // HTTP(s) request ?
  if (!(options.url).startsWith("http")) {
    return;
  }

  if (shouldCancelEarly(tab, options)) {
    // We need to cancel early to prevent multiple reopenings
    return {cancel: true};
  }

  reopenTab({
    url: options.url,
    tab,
    cookieStoreId
  });

  return {cancel: true};
}

(async function init() {
  await setupMACAddonManagementListeners(); // Listen for Multi-Account Container addon changes
  macAddonEnabled = await isMACAddonEnabled(); // Check if Multi-Account Container is enabled (it must be!)

  try {
    await setupContainer(); // Check for existing container or create a new one
  } catch (error) {
    return;
  }

  // Clean up canceled requests
  browser.webRequest.onCompleted.addListener((options) => {
    if (canceledRequests[options.tabId]) {
      delete canceledRequests[options.tabId];
    }
  },{urls: ["<all_urls>"], types: ["main_frame"]});

  browser.webRequest.onErrorOccurred.addListener((options) => {
    if (canceledRequests[options.tabId]) {
      delete canceledRequests[options.tabId];
    }
  },{urls: ["<all_urls>"], types: ["main_frame"]});

  // Add the request listener
  browser.webRequest.onBeforeRequest.addListener(containUnsafe, {urls: ["<all_urls>"], types: ["main_frame"]}, ["blocking"]);

  // Process current tabs
  processCurrentTabs();
})();
