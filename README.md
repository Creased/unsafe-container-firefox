# Unsafe Container

Unsafe Container is based on the [Firefox Multi-Account Containers](https://support.mozilla.org/kb/containers) addon and places all your web activity in a container named "Unsafe Container" and allows you to move it afterwards.

## Installation

1. Install [Firefox Multi-Account Containers](https://addons.mozilla.org/firefox/addon/multi-account-containers/).
2. Install [Unsafe Container](https://addons.mozilla.org/firefox/addon/unsafe-container/)
3. Open Firefox and use it like you normally would.  Firefox will automatically switch to the Unsafe Container tab for you and let you move your traffic to another container if you want.

*Happy Hacking!*